#include "Fighter.h"

Fighter::Fighter(AnimationSet* animSet, float x, float y, bool direction){
	this->animSet = animSet;
	this->x = x;
	this->y = y;
	this->direction = direction;

	hp = hpMax = 100;

	changeAnimation(FC::fs_idle, true);
	updateCollisionBox();

}

void Fighter::update(){
	//update collision boxes
	updateCollisionBox();
	//update movement/input
	updateMovement();

	//bump into stuff
	updateCollisions();

	//did we crash?
	if (falling && onGround)
	{
		falling = false;
		changeAnimation(FC::fs_crashed, true);
	}
	//are we in the air and not in inAir state and not busy, then fix animations and state
	if (!onGround && (state != FC::fs_inAir || checkBusy())){
		changeAnimation(FC::fs_inAir, true);
	}

	//update animations
	updateAnimation();

	//update timers and things
	updateInvincibleTimer();
}

void Fighter::changeAnimation(int newState, bool resetFrameToBegging){
	state = newState;
	if (state == FC::fs_idle){
		//idle is my default, so i'll use this one to check if it should change to a more accurate one, just to be careful
		if (crouching)  
			state = FC::fs_crouch;
		else if (moving && !running)
			state = FC::fs_walk;
		else if (moving && running)
			state = FC::fs_run;
		else if (!onGround)
			state = FC::fs_inAir;
		else
		{
			//must actually be idling i guess
			currentAnim = animSet->getAnimation(FC::fba_idle);

		}
	}
	//NOTE: if statement stack NOT connected on purpose
	if (state == FC::fs_crouch){
		currentAnim = animSet->getAnimation(FC::fba_crouch);
	}
	if (state == FC::fs_walk){
		currentAnim = animSet->getAnimation(FC::fba_walk);
		stopBlocking();
	}
	if (state == FC::fs_run){
		currentAnim = animSet->getAnimation(FC::fba_run);
	}
	if (state == FC::fs_walkBackwards){
		currentAnim = animSet->getAnimation(FC::fba_walkBackwards);
	}
	if (state == FC::fs_inAir){
		currentAnim = animSet->getAnimation(FC::fba_inAir);
	}
	if (state == FC::fs_hurt && onGround){
		currentAnim = animSet->getAnimation(FC::fba_hurt);
		stopBlocking();
	}
	if (state == FC::fs_hurt && !onGround){
		currentAnim = animSet->getAnimation(FC::fba_hurtInAir);
		stopBlocking();
	}

	if (resetFrameToBegging)
		currentFrame = currentAnim->getFrame(0);
	else
		currentFrame = currentAnim->getFrame(currentFrame->frameNumber); //change direction for example, wanna change animation, but not what frame we were on

}
bool Fighter::checkBusy(){
	if (state == FC::fs_action || state == FC::fs_hurt || state == FC::fs_crashed)
		return true;
	else
		return false;
}

void Fighter::doMove(FighterMove &move){
	state = FC::fs_action;
	stopBlocking();

	currentAnim = animSet->getAnimation(move.moveName);
	currentFrame = currentAnim->getFrame(0);
	if (move.enCost > 0)
	{
		en -= move.enCost;
		if (en < 0)
			en = 0;
	}
}

void Fighter::addChain(FighterMove &move){
	nextChainMove = &move;
}
void Fighter::moveInDirection(bool dir, bool run){
	//only do if not busy
	if (checkBusy())
		return;
	
	//if moving forward
	float mspeed = walkSpeed;
	float mangle = 0;
	if (dir == FC::fd_right)
		mangle = 0;
	else
		mangle = 180;

	if (crouching)
	{
		//dont change if crouching
		mspeed = 0;
	}
	else if (onGround){
		//if on ground then
		if(direction == dir){
			//cant block if moving forward
			stopBlocking();

			//if trying to move in the direction they are facing
			if (run){
				//if running
				mspeed = runSpeed;
				bool resetAnimation = (state != FC::fs_run);//reset if not running
				changeAnimation(FC::fs_run, resetAnimation);
			}
			else
			{
				//else walk
				bool resetAnimation = (state != FC::fs_walk);
				changeAnimation(FC::fs_walk, resetAnimation);
			}
		}
		else
		{
			//must be moving backwards
			mspeed = walkSpeed;
			bool resetAnimation = (state != FC::fs_walkBackwards);
			changeAnimation(FC::fs_walkBackwards, resetAnimation);

		}
	}
	move(angle, mspeed);
}
void Fighter::crouch(){
	moving = false;
	bool resetAnimation = (state != FC::fs_crouch);
	changeAnimation(FC::fs_crouch, resetAnimation);
}

void Fighter::setBlocking(){
	if (!onGround)
		blocking = FC::fbt_all; //block all from air, up to you to change this rule
	else if (crouching)
		blocking = FC::fbt_low;
	else
		blocking = FC::fbt_high;
}

void Fighter::stopBlocking(){
	blocking = FC::fbt_none;
}

void Fighter::jump(float amount){
	if (!onGround)
		return;

	pushAmount = amount;
	pushAngle = 90; //up!
}

bool Fighter::checkHit(SDL_Rect hitbox, FC::FighterHitType hitType){
	//if invinceble, cant get hit
	if (invincibleTimer > 0)
		return false;
	
	SDL_Rect hitIntersect;
	bool hitConnected = SDL_IntersectRect(&collisionBox, &hitbox, &hitIntersect);
	if (hitConnected)
	{
		//if not blocking OR unblockable, too bad, hits regardless
		if (blocking == FC::fbt_none || hitType == FC::fht_unblockable)
		{
			return true;
		}
		//if the hit is low and we're blocking high
		if (blocking == FC::fbt_high && hitbox.y + (hitbox.h / 2.0f) > collisionBox.y + (collisionBox.h / 2.0f))
			return true;
		//if hit high when blocking low
		if (blocking == FC::fbt_low && hitbox.y + (hitbox.h / 2.0f) < collisionBox.y + (collisionBox.h / 2.0f))
			return true;


		//otherwise we've blocked this
		return false;
	}
	//or it missed completely
	return false;
}

void Fighter::takeDamage(int damage, FC::FighterHitType hitType, float angle){
	if (invincibleTimer <= 0){
		hp -= damage;
		if (hp < 0)
			hp = 0;

		if (hp == 0)
			falling = true;

		pushAngle = angle;
		if (hitType == FC::fht_small)
			pushAmount = 20;
		if (hitType == FC::fht_medium)
			pushAmount = 60;
		if (hitType == FC::fht_heavy){
			pushAmount = 120;
			falling = true;
		}

		invincibleTimer = 0.3;
		changeAnimation(FC::fs_hurt, true);
		
	}
}


void Fighter::updateAnimation(){
	if (currentFrame == NULL || currentAnim == NULL)
		return; //cant do much with no frame or no animation


	frameTimer += TimeController::timerController.dT;
	//time to change frames
	if (frameTimer >= currentFrame->duration)
	{
		//if at the end of the animation
		if (currentFrame->frameNumber == currentAnim->getEndFrameNumber())
		{
			//depends on current animation and whats going on a bit
			if (state == FC::fs_action){
				//action done, go back to idle, unless we're chaining
				if (nextChainMove != NULL)
				{ 
					doMove(*nextChainMove); //do chain
					nextChainMove = NULL; //reset to null so we dont infinite attack loop
				}
				else
				{
					changeAnimation(FC::fs_idle, true);
				}
			}
			else if ( state == FC::fs_hurt && !falling){
				//hurt done, go back to idle
				if (onGround)
					changeAnimation(FC::fs_idle, true);
				else
					changeAnimation(FC::fs_inAir, true);
			}
			else if (state == FC::fs_crashed)
			{
				if (hp > 0)//get back up if we're not defeated)
				{
					frameTimer = 0;
					//if some how alive again, then change state back to moving
					changeAnimation(FC::fs_idle, true);
				}//else, stay in final crashed frame
				else
				{
					frameTimer = 0;
				}
			}
			else
			{
				//just reset the animation
				frameTimer = 0;
				currentFrame = currentAnim->getFrame(0);
			}

		}
		else
		{
			//just move to the next frame in the animation
			currentFrame = currentAnim->getNextFrame(currentFrame);
		}
		frameTimer = 0; //crucial step. If we miss this, the next frame skips real quick
	}
}