#ifndef AnimationSet_header
#define AnimationSet_header

#include <iostream>
#include <fstream>
#include <list>
#include "SDL.h"
#include "globals.h"
#include "Animation.h"

using namespace std;

class AnimationSet
{
public:

	string outputFileName;
	string imageName;
	SDL_Texture *spriteSheet; //this is where we load this one up and then pass it down the line. Meaning here is the class we also have to destroy it in here
	SDL_Texture *whiteSpriteSheet = NULL;//I use this to show damage. Draw the one frame when chara is invincible
	list<Animation> animations;
	//Functions:
	AnimationSet(){ ; } //generic one used for load/save
	~AnimationSet(); //- properly cleans up spriteSheet
	
	//loads a fdset file, also takes a list of what types of data we expect to loading.
	//Also, if we're working with an 8bit image and we know in that 8bit image what coloured pixel in the palette 
	//we would prefer to be transparent, then we can say yes to 'colorKeying'(make a colour in the palette actually equal full transparent)
	//and then just use the index of that transparentPixel (e.g, if its the third colour in the palette, then put in 2 as index starts at 0)
	//if you need an alternative white version of the sprite sheet, then make this last option true (maybe move this out of this class? not sure)
	void loadAnimationSet(string fileName, list<DataGroupType> &groupTypes, bool setColourKey = false, int transparentPixelIndex = 0, bool createWhiteTexture = false);

	
	//helpers
	Animation* getAnimation(string name); //� gets an animation by name(can return null if no match)
};

#endif