#include "entity.h"

//statics/constants

list<Entity *> Entity::entities;

void Entity::update(){
	//override me if you want logic to happen
}

void Entity::draw(){
	//override me if you want something more specific to happen
	//renderTexture(animSet->frameSheet, Globals::renderer, x - currentFrame->xOffset, y - currentFrame->yOffset, &currentFrame->clip);
	if (currentFrame != NULL && active){
		currentFrame->Draw(animSet->spriteSheet, x, y);
	}
	if (solid && Globals::debugging)
	{
		SDL_SetRenderDrawColor(Globals::renderer, 255, 0, 0, 255);
		SDL_RenderDrawRect(Globals::renderer, &collisionBox);
	}
}


void Entity::move(float angle)
{
	moving = true;
	moveSpeed = moveSpeedMax;
	this->angle = angle;



}

void Entity::move(float angle, float speed)
{
	moving = true;
	moveSpeed = speed;
	this->angle = angle;



}

void Entity::updateMovement(){

	//setup collisionBoxes for where we are now
	//updateCollisionBox();

	//reset these back to 0
	totalXMove = 0;
	totalYMove = 0;

	if (moveSpeed > 0)
	{
		float movedist = moveSpeed*(TimeController::timerController.dT)*moveLerp;

		if (movedist > 0)
		{
			float xMove = movedist*(cos(angle*Globals::PI / 180));
			float yMove = movedist*(sin(angle*Globals::PI / 180));

			x += xMove;
			y += yMove;

			totalXMove = xMove;
			totalYMove = yMove;

			if (!moving)
				moveSpeed -= movedist;
		}
		else if (!moving)
		{
			moveSpeed = 0;
		}
	}
	if (pushAmount > 0){
		float pushDist = pushAmount*TimeController::timerController.dT*moveLerp;
		if (pushDist > 0){
			float xMove = pushDist*(cos(pushAngle*Globals::PI / 180));
			float yMove = pushDist*(sin(pushAngle*Globals::PI / 180));

			x += xMove;
			y += yMove;

			totalXMove += xMove;
			totalYMove += yMove;
			pushAmount -= pushDist;
		}
		else
		{
			pushAmount = 0;
		}
		
	}
	//are we in the air now but were on the ground before?
	if (adhereToGravity && onGround && y < Globals::groundLevel )
	{
		onGround = false;
		gravityPull = 0;
	}
	//And now Gravity
	if (adhereToGravity && !onGround && y < Globals::groundLevel && gravityPull < gravityMaxPull){
		gravityPull += TimeController::timerController.dT * moveLerp;
		if (gravityPull > gravityMaxPull)
			gravityPull = gravityMaxPull;

		y += gravityPull;
	}
	//maybe we've landed
	if (adhereToGravity && !onGround && y >= Globals::groundLevel)
	{
		onGround = true;
		gravityPull = 0;
		y = Globals::groundLevel;
	}

	//now, we get the new collisionbox for where we have moved to
	updateCollisionBox();

}

void Entity::updateCollisionBox(){
	
	collisionBox.x = x - collisionBox.w / 2;
	collisionBox.y = y + collisionBoxYOffset;
}

void Entity::updateCollisions(){
	if (active && collideWithSolids && (moveSpeed > 0 || pushAmount > 0)){
		//list<Entity*> collisions;
		
		for (auto entity = Entity::entities.begin(); entity != Entity::entities.end(); entity++)
		{
			//bumping into something solid
			if ((*entity)->active && (*entity)->type != this->type && (*entity)->solid && Globals::checkCollision(collisionBox, (*entity)->collisionBox)){
				
				moveSpeed = 0;
				moving = false;

				//push us slowly out of the other entity, 1 pixel at a time each loop cycle
				if (x < (*entity)->x)
					x--;
				else
					x++;

				updateCollisionBox();
			}
		}
		//if (collisions.size() > 0){
		//	
		//	updateCollisionBox();

		//	//x -= totalXMove;
		//	//y -= totalYMove;

		//	//MULTISAMPLE CODE. Why? Because I gotta! :P

		//	//firsts, what the biggest length OUR collisionbox and then divide by 2
		//	float boxHalfSize = 0;
		//	if (collisionBox.w < collisionBox.h)
		//		boxHalfSize = collisionBox.w / 4;
		//	else
		//		boxHalfSize = collisionBox.h / 4;

		//	//ok, so the above code before just checked for any possible colliding entities on the path
		//	//now we are going to actually step by step move towards our destination and check collisions as we move through time
		//	SDL_Rect sampleBox = lastCollisionBox;
		//	float movementAngle = Globals::angleBetweenTwoSDLRect(lastCollisionBox, collisionBox);

		//	bool foundCollision = false;
		//	while (!foundCollision){
		//		//check for collisions, take a step if we haven't found any


		//		SDL_Rect intersection;
		//		for (auto entity = collisions.begin(); entity != collisions.end(); entity++)
		//		{
		//			if (SDL_IntersectRect(&sampleBox, &(*entity)->collisionBox, &intersection)){
		//				foundCollision = true;
		//				pushAngle = angleBetweenTwoEntities((*entity), this);

		//				//do something about it?
		//				//if (lastCollisionBox.x + lastCollisionBox.w / 2 != (*entity)->collisionBox.x + (*entity)->collisionBox.w / 2){
		//				if (intersection.w < intersection.h){
		//					if (lastCollisionBox.x + lastCollisionBox.w / 2 < (*entity)->collisionBox.x + (*entity)->collisionBox.w / 2)
		//						sampleBox.x -= intersection.w;
		//					else
		//						sampleBox.x += intersection.w;
		//				}
		//				//if (lastCollisionBox.y + lastCollisionBox.h / 2 != (*entity)->collisionBox.y + (*entity)->collisionBox.h / 2){
		//				else{	
		//				if (lastCollisionBox.y + lastCollisionBox.h / 2 < (*entity)->collisionBox.y + (*entity)->collisionBox.h / 2)
		//						sampleBox.y -= intersection.h;
		//					else
		//						sampleBox.y += intersection.h;
		//					
		//				}
		//			}
		//		}
		//		//break out of the collision early
		//		if (foundCollision ||(sampleBox.x == collisionBox.x && sampleBox.y == collisionBox.y))
		//			break;

		//		if (Globals::distanceBetweenTwoRects(sampleBox, collisionBox) > boxHalfSize){
		//			float xMove = boxHalfSize*(cos(movementAngle*Globals::PI / 180));
		//			float yMove = boxHalfSize*(sin(movementAngle*Globals::PI / 180));

		//			sampleBox.x += xMove;
		//			sampleBox.y += yMove;
		//		}
		//		else
		//		{
		//			sampleBox = collisionBox;
		//		}


		//	} 
		//	
		//	if (foundCollision){
		//		//move us to wherever sampleBox is now
		//		pushAmount = pushAmount / 2;
		//		x = sampleBox.x + sampleBox.w / 2;
		//		y = sampleBox.y - collisionBoxYOffset;
		//	}
			
		//}
		
	}
	updateCollisionBox();
}






//help functions


float Entity::distanceBetweenTwoEntities(Entity *e1, Entity *e2)
{
	float d = abs(sqrt((e2->x - e1->x)*(e2->x - e1->x) + (e2->y - e1->y)*(e2->y - e1->y)));
	return d;
}

float Entity::angleBetweenTwoEntities(Entity *e1, Entity *e2)
{
	float dx, dy;
	float x1 = e1->x, y1 = e1->y, x2 = e2->x, y2 = e2->y;
	if (e1->solid){
		x1 = e1->collisionBox.x + e1->collisionBox.w / 2;
		y1 = e1->collisionBox.y + e1->collisionBox.h / 2;
	}
	if (e2->solid){
		x2 = e2->collisionBox.x + e2->collisionBox.w / 2;
		y2 = e2->collisionBox.y + e2->collisionBox.h / 2;
	}

	dx = x2 - x1;
	dy = y2 - y1;


	return atan2(dy, dx) * 180 / Globals::PI;
}



//bool Entity::EntityCompare(const Entity * const & a, const Entity * const & b)
//{
//	if (a != 0 && b != 0)
//	{
//		return a->y < b->y;
//	}
//	else{
//		return false;
//	}
//
//}


void Entity::removeInactiveEntitiesFromList(list<Entity*> *entityList, bool deleteEntities){
	for (list<Entity *>::iterator i = entityList->begin(); i != entityList->end();)
	{
		if (!(*i)->active)
		{
			if (deleteEntities)
				delete (*i);
			i = entityList->erase(i);
		}
		else
		{
			++i;
		}
	}
}

void Entity::removeAllFromList(list<Entity*> *entityList, bool deleteEntities){
	 for (list<Entity *>::iterator i = entityList->begin(); i != entityList->end();)
	{
		if (deleteEntities)
			delete (*i);
		i = entityList->erase(i);
		
	}
}
