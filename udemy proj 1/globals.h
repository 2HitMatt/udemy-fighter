#ifndef GLOBALS_H
#define GLOBALS_H

#include <string>
#include <iostream>
#include <SDL.h>
#include "randomNumber.h"

using namespace std;

class Globals
{
public:
	//math helpers
	static const float PI;

	//useful for dev people
	static bool debugging;

	//sdl related
	static int tileSize;
	static int ScreenWidth, ScreenHeight, ScreenScale;
	static SDL_Renderer* renderer;
	
	//where the floor is
	static int groundLevel; 

	//other helpers
	/**cuts off the extra info. e.g hitbox: 2 23 23 23, it cuts off the 'hitbox: ' part and just leaves the numbers*/
	static string clipOffDataHeader(string str);

	static float distanceBetweenTwoRects(SDL_Rect &r1, SDL_Rect &r2);
	static bool checkCollision(SDL_Rect cbox1, SDL_Rect cbox2);
	static float angleBetweenTwoPoints(float cx1, float cy1, float cx2, float cy2);
	static float angleBetweenTwoSDLRect(SDL_Rect &col1, SDL_Rect &col2);

	
};

#endif