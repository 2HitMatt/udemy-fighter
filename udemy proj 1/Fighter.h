#ifndef FIGHTER
#define FIGHTER

#include "livingEntity.h"
#include "globals.h"
#include "TimeController.h"
#include "animationSet.h"
#include "FighterConstants.h"
#include "FighterMove.h"
using namespace std;

class Fighter : public LivingEntity{
public:
	bool direction = true;//left or right
	bool running = false;
	bool crouching = false;
	bool falling = false; //must crash onto ground to end falling or some other action
	int en, enMax; //energy points
	float walkSpeed = 4, runSpeed = 6, jumpAmount = 20;
	
	FC::FighterBlockingType blocking = FC::fbt_none;

	FighterMove *nextChainMove = NULL;

	Fighter(AnimationSet* animSet, float x, float y, bool direction);

	//VIRTUAL FUNCTIONS
	virtual void update();

	virtual void changeAnimation(int newState, bool resetFrameToBegging);
	virtual bool checkBusy();
	virtual void doMove(FighterMove &move);
	virtual void addChain(FighterMove &move);
	virtual void moveInDirection(bool dir, bool run);
	virtual void crouch();
	virtual void setBlocking();
	virtual void stopBlocking();
	virtual void jump(float amount);
	
	virtual bool checkHit(SDL_Rect hitbox, FC::FighterHitType hitType);
	virtual void takeDamage(int damage, FC::FighterHitType hitType, float angle);

	void updateAnimation();


};

#endif
