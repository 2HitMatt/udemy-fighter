#include "CharacterController.h"

void CharacterController::addCommand(string command){
	commands.append(command);
	inputTimeoutTimer = 0;
}


void CharacterController::updateInputTimer(){
	if (commands != ""){
		inputTimeoutTimer += TimeController::timerController.dT;
		if (inputTimeoutTimer >= inputMaxTime)
		{
			//ran out of time to work out the move, reset the timers and empty out the commands
			commands = "";
			inputTimeoutTimer = 0;
		}
	}
}

void CharacterController::checkCommandsForMoves(){
	//do any of the moves in my movelist match the current set of commands
	for (list<FighterMove>::iterator fm = moves.begin(); fm != moves.end(); fm++){
		if (commands.find(fm->commands, 0) != -1){
			//there is a match, but does it meet the other requirements

			//enough energy? if chain move, are chaining onto the right move? or is it a cancel move and is it past the cancel frame point?
			if (fm->enCost <= fighter->en && (fm->chainOntoMove == "" || fm->chainOntoMove == fighter->currentAnim->name) && (fm->cancelMove == "" || (fm->cancelMove <= fighter->currentAnim->name && fm->cancelFrame == fighter->currentFrame->frameNumber)))
			{
				doMove((*fm));
				commands = "";
				inputTimeoutTimer = 0;

				break;
			}
		}
	}
	//didn't find anything, dont worry about it
}
void CharacterController::doMove(FighterMove fm){
	if (fighter != NULL)
		fighter->doMove(fm);
}
void CharacterController::walk(bool dir){
	fighter->moveInDirection(dir, false);
}
void CharacterController::checkCommandsForRun(bool dir){
	string runCommand; //depends on direction
	if (dir == FC::fd_right)
		runCommand = FC::C_RIGHT + FC::C_RIGHT;
	else
		runCommand = FC::C_LEFT + FC::C_LEFT;

	//does the run command exist in the last 2 positions(most current)
	if (commands.find(runCommand, commands.length()-2) != -1)
	{
		run(dir);//found? then run!
	}
}

void CharacterController::run(bool dir){
	fighter->moveInDirection(dir, true);
}
void CharacterController::jump(){
	fighter->jump(fighter->jumpAmount);
}
void CharacterController::crouch(){
	if (fighter->onGround)
		fighter->crouching = true;
}