#ifndef ENTITY
#define ENTITY

#include "globals.h"
#include "TimeController.h"
#include "animationSet.h"

//abstract class. Gonna use it for POLYMORPHISM :D. This is our base for everything in our little world
class Entity{
public:
	//all entities keep track of a state. This is a quick label to say what the entity is up to
	int state;

	//from hero to ui, still need to position stuff and turn it off and on(active)
	float x, y;
	bool solid = true; //is this thing a hard object or can things pass through it ( most are hard by default)
	bool collideWithSolids = true; //sometimes we want to be solid, but we ourselves, pass through other solids
	bool active = true; //entity turned off or on
	string type = "entity"; //what type of thing is this entity e.g hero, enemy, wall etc
	bool moving; //yes or no
	float angle; //angle to move hero
	float moveSpeed; //speed to move hero in direction
	float moveSpeedMax;
	float pushAngle; //direction pushed in
	float pushAmount; //amount of push in that direction
	float moveLerp = 4;
	float totalXMove, totalYMove; //keeps track of movement amount this turn. used just in case we want to retract our move
	bool adhereToGravity = true; //does gravity pull us down?
	float gravityPull; //how hard is gravity pulling us now!
	float gravityMaxPull = 4; //whats the fastest this thing will fall (fake floaty objects)
	bool onGround = false; //yes resting on ground, no up in the air


	SDL_Rect collisionBox;//a region describing the solid parts of the entity
	float collisionBoxYOffset; //from entities y, how should I move the collision box?


	//everything in the world has animations and stuff right?
	AnimationSet *animSet;
	Animation *currentAnim;
	Frame *currentFrame;
	float frameTimer;

	//VIRTUAL FUNCTIONS
	virtual void update();
	virtual void draw();

	virtual void move(float angle);
	virtual void move(float angle, float speed);
	virtual void updateMovement();
	virtual void updateCollisionBox();

	virtual void changeAnimation(int newState, bool resetFrameToBegging) = 0;
	virtual void updateCollisions(); //how do we bump into the world around us is different per class too

	//helper functions for the class
	static float distanceBetweenTwoEntities(Entity *e1, Entity *e2);
	static float angleBetweenTwoEntities(Entity *e1, Entity *e2);

	//static, means there is only one entities list ever made in the whole program. 
	//It belongs to the class as a whole, not a single object built from a class
	//We will chuck allll of our entities into this bad boy list, so we can sort them and cascade over them easily
	//NOTE: this is a list of pointers, we won't delete entities in this list, refer to each entities list for deleting purposes
	static list<Entity*> entities;

	//this function helps erase inactive entities from a list
	static void removeInactiveEntitiesFromList(list<Entity*> *entityList, bool deleteEntities);
	//helps empty a list
	static void removeAllFromList(list<Entity*> *entityList, bool deleteEntities);
};

#endif