#include "globals.h"


const float Globals::PI = 3.14159;

bool Globals::debugging = false;

SDL_Renderer* Globals::renderer = NULL;
int Globals::tileSize = 32;
int Globals::ScreenWidth = 640, Globals::ScreenHeight = 352, Globals::ScreenScale = 2;
int Globals::groundLevel = 320;

string Globals::clipOffDataHeader(string str){
	int pos = str.find(":", 0);
	if (pos != -1){
		str = str.substr(pos + 1, str.length() - pos + 2);
	}
	return str;
}

float Globals::distanceBetweenTwoRects(SDL_Rect &r1, SDL_Rect &r2)
{
	SDL_Point e1, e2;
	e1.x = r1.x + r1.w / 2;
	e1.y = r1.y + r1.h / 2;

	e2.x = r2.x + r2.w / 2;
	e2.y = r2.y + r2.h / 2;

	float d = abs(sqrt((e2.x - e1.x)*(e2.x - e1.x) + (e2.y - e1.y)*(e2.y - e1.y)));
	return d;
}

bool Globals::checkCollision(SDL_Rect cbox1, SDL_Rect cbox2)
{
	SDL_Rect inter;
	if (SDL_IntersectRect(&cbox1, &cbox2, &inter))
	{
		return true;
	}
	else{
		return false;
	}
	//horizontal first		
	//otherwise, false
	return false;
}


float Globals::angleBetweenTwoPoints(float cx1, float cy1, float cx2, float cy2)
{

	//angle math stuff
	float dx = cx2 - cx1;
	float dy = cy2 - cy1;

	return atan2(dy, dx) * 180 / Globals::PI;
}

float Globals::angleBetweenTwoSDLRect(SDL_Rect &col1, SDL_Rect &col2)
{
	//get the centre x and y of each rectangle
	float cx1 = col1.x + (col1.w / 2);
	float cy1 = col1.y + (col1.h / 2);
	float cx2 = col2.x + (col2.w / 2);
	float cy2 = col2.y + (col2.h / 2);

	//angle math stuff
	/*float dx = cx2 - cx1;
	float dy = cy2 - cy1;

	return atan2(dy, dx) * 180 / Globals::PI;*/
	return angleBetweenTwoPoints(cx1, cy1, cx2, cy2);
}
