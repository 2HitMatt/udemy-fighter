#include "CharacterKeyboardController.h"

CharacterKeyboardController::CharacterKeyboardController(){
	UP = SDL_SCANCODE_UP;
	DOWN = SDL_SCANCODE_DOWN;
	LEFT = SDL_SCANCODE_LEFT;
	RIGHT = SDL_SCANCODE_RIGHT;
	A = SDL_SCANCODE_Z;
	B = SDL_SCANCODE_X;
	C = SDL_SCANCODE_C;
}

void CharacterKeyboardController::update(SDL_Event* e){
	//button presses
	if (e->type == SDL_KEYDOWN){
		//TODO check directional keys
		if (e->key.keysym.scancode == UP)
		{
			addCommand(FC::C_UP);
			if (fighter->onGround && !fighter->checkBusy())
				jump();
		}
		if (e->key.keysym.scancode == DOWN)
		{
			addCommand(FC::C_DOWN);
			//will let holding down control crouch
		}
		if (e->key.keysym.scancode == RIGHT)
		{
			addCommand(FC::C_RIGHT);
			if (FC::fd_right == fighter->direction){
				//check to start run
				checkCommandsForRun(FC::fd_right);
			}
			//will check blocking while holding a direction
		}
		if (e->key.keysym.scancode == LEFT)
		{
			addCommand(FC::C_LEFT);
			if (FC::fd_left == fighter->direction){
				//check to start run
				checkCommandsForRun(FC::fd_left);
			}
			//will check blocking while holding a direction
		}

		//check action keys
		if (e->key.keysym.scancode == A)
		{
			addCommand(FC::C_A);
		}
		if (e->key.keysym.scancode == B)
		{
			addCommand(FC::C_B);
		}
		if (e->key.keysym.scancode == C)
		{
			addCommand(FC::C_C);
		}
		//if any of these main buttons were pressed, check for moves
		if (e->key.keysym.scancode == A || e->key.keysym.scancode == B || e->key.keysym.scancode == C)
		{
			checkCommandsForMoves();
		}


		//button holds
		//check for keys still being held
		const Uint8 *keystates = SDL_GetKeyboardState(NULL);
		//if hero not able to move or no directions pressed, then stop moving (still slide to a halt though)
		if ((!fighter->checkBusy()) || (!keystates[UP] && !keystates[DOWN] && !keystates[RIGHT] && !keystates[LEFT]))
		{
			fighter->moving = false;
		}
		else
		{
			//ups
			if (keystates[UP])
			{
				//dunno, dont need to do anything here
			}
			//downs
			if (keystates[DOWN])
			{
				if (fighter->onGround && !fighter->checkBusy())
				{
					fighter->crouch();
				}
			}
			//left or right
			if (keystates[LEFT] || keystates[RIGHT])
			{
				//ok, lets setup some variables nice and easy to work this out without redoing a lot of code
				bool moveDirection;
				if (keystates[RIGHT])
					moveDirection = FC::fd_right;
				else
					moveDirection = FC::fd_left;

				//still going the way we are going
				if (fighter->direction == moveDirection)
				{
					//dont change, i think
					if (!fighter->crouching)
						fighter->moveInDirection(moveDirection, fighter->running);
				}
				else
				{
					//TODO
					//check if facing right direction somehow

					//going the opposite direction (moving away from the other player, never as a run)
					if (!fighter->crouching)
						fighter->moveInDirection(moveDirection, false);
					//movin backwards now, dont run, also block
					fighter->setBlocking();
				}

			}
		}
	}
}