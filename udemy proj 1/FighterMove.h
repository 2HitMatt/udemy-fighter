#ifndef FIGHTERMOVE
#define FIGHTERMOVE

#include <string>

using namespace std;

class FighterMove{
public:
	string moveName;
	string commands;
	int state;//what state must player be in to do this move
	int enCost; //does this move cost energy to do

	string chainOntoMove; //follow which move
	string cancelMove; //cancels a move matching this name
	int cancelFrame; //cancels after this frame of cancelMove

};


#endif