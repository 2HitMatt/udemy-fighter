#include "livingEntity.h"

void LivingEntity::updateInvincibleTimer(){
	if (invincibleTimer > 0)
		invincibleTimer -= TimeController::timerController.dT;
}

void LivingEntity::draw(){
	if (currentFrame != NULL && active){
		if (invincibleTimer > 0 && animSet->whiteSpriteSheet != NULL)
			currentFrame->Draw(animSet->whiteSpriteSheet, x, y);
		else
			currentFrame->Draw(animSet->spriteSheet, x, y);
	}
	if (solid && Globals::debugging)
	{

		SDL_SetRenderDrawColor(Globals::renderer, 255, 0, 0, 255);
		SDL_RenderDrawRect(Globals::renderer, &collisionBox);

		SDL_SetRenderDrawColor(Globals::renderer, 255, 0, 255, 255);
		SDL_RenderDrawRect(Globals::renderer, &hitBox);
	}
}