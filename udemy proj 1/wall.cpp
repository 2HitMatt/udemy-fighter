#include "wall.h"

Wall::Wall(AnimationSet *animSet){
	this->animSet = animSet;
	changeAnimation(1, true);

	//collisionBox setup
	solid = true;
	
	collisionBoxW = 32;
	collisionBoxH = 32;
	collisionBox.w = collisionBoxW;
	collisionBox.h = collisionBoxH;

	collisionBoxYOffset = -16;
}
void Wall::update(){
	updateCollisionBox();

	//just update animation here
	if (currentFrame == NULL || currentAnim == NULL)
		return; //cant do much with no frame or no animation
	frameTimer += TimeController::timerController.dT;
	//time to change frames
	if (frameTimer >= currentFrame->duration)
	{
		//if at the end of the animation
		if (currentFrame->frameNumber == currentAnim->getEndFrameNumber())
		{
			//just reset the animation
			currentFrame = currentAnim->getFrame(0);

		}
		else
		{
			//just move to the next frame in the animation
			currentFrame = currentAnim->getNextFrame(currentFrame);
		}
		frameTimer = 0; //crucial step. If we miss this, the next frame skips real quick
	}
}

void Wall::changeAnimation(int newState, bool resetFrameToBegging){
	//dont care about the parameters here, just set wall anim to the only wall animation. Update this if you want to break your walls or
	//give them multiple states/animations
	currentAnim = animSet->getAnimation("wall");
	currentFrame = currentAnim->getFrame(0);
}