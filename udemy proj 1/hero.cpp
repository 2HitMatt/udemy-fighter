#include "hero.h"


const string Hero::HERO_ANIM_UP = "up";
const string Hero::HERO_ANIM_DOWN = "down";
const string Hero::HERO_ANIM_LEFT = "left";
const string Hero::HERO_ANIM_RIGHT = "right";
const string Hero::HERO_ANIM_IDLE_UP = "idleUp";
const string Hero::HERO_ANIM_IDLE_DOWN = "idleDown";
const string Hero::HERO_ANIM_IDLE_LEFT = "idleLeft";
const string Hero::HERO_ANIM_IDLE_RIGHT = "idleRight";
const string Hero::HERO_ANIM_SLASH_UP = "slashUp";
const string Hero::HERO_ANIM_SLASH_DOWN = "slashDown";
const string Hero::HERO_ANIM_SLASH_LEFT = "slashLeft";
const string Hero::HERO_ANIM_SLASH_RIGHT = "slashRight";
const string Hero::HERO_ANIM_DASH_UP = "dashUp";
const string Hero::HERO_ANIM_DASH_DOWN = "dashDown";
const string Hero::HERO_ANIM_DASH_LEFT = "dashLeft";
const string Hero::HERO_ANIM_DASH_RIGHT = "dashRight";
const string Hero::HERO_ANIM_DIE = "die";



const int Hero::HERO_STATE_IDLE = 0;
const int Hero::HERO_STATE_MOVE = 1;
const int Hero::HERO_STATE_SLASH = 2;
const int Hero::HERO_STATE_DASH = 3;
const int Hero::HERO_STATE_DEAD = 4;

Hero::Hero(AnimationSet *animSet){
	this->animSet = animSet;

	type = "hero";

	//setup defaults
	x = Globals::ScreenWidth / 2;
	y = Globals::ScreenHeight / 2;
	moveSpeed = 0;
	moveSpeedMax = 80;
	hp = hpMax = 20;
	damage = 0;
	collisionBoxW = 20;
	collisionBoxH = 24;
	collisionBox.w = collisionBoxW;
	collisionBox.h = collisionBoxH;
	collisionBoxYOffset = -20;

	direction = DIR_DOWN;

	changeAnimation(HERO_STATE_IDLE, true);

	updateCollisionBox();
}

void Hero::update(){
	//check if died
	if (hp < 1 && state != HERO_STATE_DEAD){
		changeAnimation(HERO_STATE_DEAD, true);
		moving = false;
		die();
	}
	
	//update collision boxes
	updateCollisionBox();
	//update movement/input
	updateMovement();

	//bump into stuff
	updateCollisions();
	
	//only care about damage hitboxes after we're landed on a final spot in our code
	updateHitBox();
	updateDamages();

	//update animations
	updateAnimation();

	//update timers and things
	updateInvincibleTimer();
}


void Hero::slash(){
	if (hp > 0 && state == HERO_STATE_MOVE || state == HERO_STATE_IDLE)
	{
		moving = false;
		frameTimer = 0;
		state = HERO_STATE_SLASH;
		changeAnimation(state, true);
		SoundManager::soundManager.playSound("swing");
	}
}
void Hero::dash(){
	if (hp > 0 && state == HERO_STATE_MOVE || state == HERO_STATE_IDLE)
	{
		moving = false;
		frameTimer = 0;
		state = HERO_STATE_DASH;

		slideAngle = angle;
		slideAmount = 300;
		invincibleTimer = 0.1;
		changeAnimation(state, true);
		SoundManager::soundManager.playSound("dash");
	}
}

void Hero::die(){
	moving = false;
	state = HERO_STATE_DEAD;
	changeAnimation(state, true);
}

void Hero::revive(){
	hp = hpMax;
	changeAnimation(HERO_STATE_IDLE, true);
	moving = false;
	x = Globals::ScreenWidth / 2;
	y = Globals::ScreenHeight / 2;
	slideAmount = 0;
}

void Hero::changeAnimation(int newState, bool resetFrameToBegging){
	state = newState;

	if (state == HERO_STATE_IDLE)
	{
		if (direction == DIR_DOWN)
			currentAnim = animSet->getAnimation(HERO_ANIM_IDLE_DOWN);
		if (direction == DIR_UP)
			currentAnim = animSet->getAnimation(HERO_ANIM_IDLE_UP);
		if (direction == DIR_LEFT)
			currentAnim = animSet->getAnimation(HERO_ANIM_IDLE_LEFT);
		if (direction == DIR_RIGHT)
			currentAnim = animSet->getAnimation(HERO_ANIM_IDLE_RIGHT);
	}
	else if (state == HERO_STATE_MOVE)
	{
		if (direction == DIR_DOWN)
			currentAnim = animSet->getAnimation(HERO_ANIM_DOWN);
		if (direction == DIR_UP)
			currentAnim = animSet->getAnimation(HERO_ANIM_UP);
		if (direction == DIR_LEFT)
			currentAnim = animSet->getAnimation(HERO_ANIM_LEFT);
		if (direction == DIR_RIGHT)
			currentAnim = animSet->getAnimation(HERO_ANIM_RIGHT);
	}
	else if (state == HERO_STATE_SLASH)
	{
		if (direction == DIR_DOWN)
			currentAnim = animSet->getAnimation(HERO_ANIM_SLASH_DOWN);
		if (direction == DIR_UP)
			currentAnim = animSet->getAnimation(HERO_ANIM_SLASH_UP);
		if (direction == DIR_LEFT)
			currentAnim = animSet->getAnimation(HERO_ANIM_SLASH_LEFT);
		if (direction == DIR_RIGHT)
			currentAnim = animSet->getAnimation(HERO_ANIM_SLASH_RIGHT);
	}
	else if (state == HERO_STATE_DASH)
	{
		if (direction == DIR_DOWN)
			currentAnim = animSet->getAnimation(HERO_ANIM_DASH_DOWN);
		if (direction == DIR_UP)
			currentAnim = animSet->getAnimation(HERO_ANIM_DASH_UP);
		if (direction == DIR_LEFT)
			currentAnim = animSet->getAnimation(HERO_ANIM_DASH_LEFT);
		if (direction == DIR_RIGHT)
			currentAnim = animSet->getAnimation(HERO_ANIM_DASH_RIGHT);
	}
	else if (state == HERO_STATE_DEAD)
	{
		//always faces the person watching. If directional death, do the same as above
		currentAnim = animSet->getAnimation(HERO_ANIM_DIE);
	}

	if (resetFrameToBegging)
		currentFrame = currentAnim->getFrame(0);
	else
		currentFrame = currentAnim->getFrame(currentFrame->frameNumber); //change direction for example, wanna change animation, but not what frame we were on

}


void Hero::updateAnimation(){
	if (currentFrame == NULL || currentAnim == NULL)
		return; //cant do much with no frame or no animation

	//if in a moving state, but not actually moving, return to idle 
	if (state == HERO_STATE_MOVE && !moving){
		changeAnimation(HERO_STATE_IDLE, true);
	}
	//if should show running animation, lets change the state properly
	if (state != HERO_STATE_MOVE && moving){
		changeAnimation(HERO_STATE_MOVE, true);
	}

	frameTimer += TimeController::timerController.dT;
	//time to change frames
	if (frameTimer >= currentFrame->duration) 
	{
		//if at the end of the animation
		if (currentFrame->frameNumber == currentAnim->getEndFrameNumber())
		{
			//depends on current animation and whats going on a bit
			if (state == HERO_STATE_SLASH || state == HERO_STATE_DASH){
				//slash done, go back to moving
				changeAnimation(HERO_STATE_MOVE, true);
			}
			else if (state == HERO_STATE_DEAD)
			{
				frameTimer = 0;
				//if some how alive again, then change state back to moving
				if (hp > 0)
					changeAnimation(HERO_STATE_MOVE, true);
			}
			else
			{
				//just reset the animation
				currentFrame = currentAnim->getFrame(0);
			}
			
		}
		else
		{
			//just move to the next frame in the animation
			currentFrame = currentAnim->getNextFrame(currentFrame);
		}
		frameTimer = 0; //crucial step. If we miss this, the next frame skips real quick
	}
}
void Hero::updateDamages(){
	if (active && hp > 0 && invincibleTimer <= 0){
		for (auto entity = Entity::entities.begin(); entity != Entity::entities.end(); entity++)
		{
			if ((*entity)->active && (*entity)->type == "enemy"){
				//reference as livingEntity, so we can access enemies hit box
				LivingEntity* enemy = (LivingEntity*)(*entity);

				if (enemy->damage > 0 && Entity::checkCollision(collisionBox, enemy->hitBox)){


					hp -= enemy->damage;
					if (hp > 0){
						invincibleTimer = 0.3;
						SoundManager::soundManager.playSound("hit");
					}

					//angle from other entity, to towards this entity
					slideAngle = Entity::angleBetweenTwoEntities((*entity), this);
					slideAmount = 200;
				}
			}
		}
	}
}