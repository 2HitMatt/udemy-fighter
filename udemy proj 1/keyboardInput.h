#ifndef KEYBOARDINPUT
#define KEYBOARDINPUT

#include "globals.h"
#include "hero.h"

using namespace std;

class KeyboardInput{
public:
	Hero* hero;
	SDL_Scancode UP, DOWN, LEFT, RIGHT;
	SDL_Scancode SLASH, DASH;

	KeyboardInput();
	void update(SDL_Event* e);

};


#endif