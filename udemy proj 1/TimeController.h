#ifndef TIMECONTROLLER
#define TIMECONTROLLER

#include <iostream>
#include <SDL.h>

using namespace std;

class TimeController{
public:
	static int PLAY_STATE, PAUSE_STATE;

	int timeState;
	Uint32 lastUpdate;
	float dT; //delta time, in seconds (0.001 probably the average)

	TimeController();
	void updateTime();
	void pause();
	void resume();
	void reset();


	static TimeController timerController;
};

#endif
