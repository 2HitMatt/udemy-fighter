#include <iostream>
#include <sstream>
#include <ctime> 
#include <cstdlib>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>
#include "res_path.h"
#include "cleanup.h"
#include "drawing_functions.h"
#include "globals.h"
#include "game.h"

//THE TODO LIST
/*
KEY
- top level
-- sub level
+ needed for first alpha
* do next!

Bugs

Battle


FUN STUFF AFTER THE ABOVE IS DONE

*/

int main(int, char**){
	//set random seed for random number sequence
	srand(time(0));

	if (SDL_Init(SDL_INIT_EVERYTHING) != 0){//everything??? | SDL_INIT_GAMECONTROLLER
		system("pause");
		return 1;
	}

	SDL_Window *window = SDL_CreateWindow("Udemy Project 1", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, Globals::ScreenWidth*Globals::ScreenScale,
		Globals::ScreenHeight*Globals::ScreenScale, SDL_WINDOW_SHOWN);//| SDL_WINDOW_FULLSCREEN
	if (window == nullptr){
		SDL_Quit();
		system("pause");
		return 1;
	}
	

	Globals::renderer = SDL_CreateRenderer(window, -1,
		SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (Globals::renderer == nullptr){
		cleanup(window);
		SDL_Quit();
		system("pause");
		return 1;
	}


	

	//res independant
	SDL_RenderSetLogicalSize(Globals::renderer, Globals::ScreenWidth, Globals::ScreenHeight);


	if ((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG){
		SDL_Quit();
		system("pause");
		return 1;
	}
	if (TTF_Init() != 0){
		SDL_Quit();
		system("pause");
		return 1;
	}
	//Initialize SDL_mixer
	if (Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096) == -1)
	{
		SDL_Quit();
		system("pause");
		return 1;
	}

	//load bmp int sdl_surface(can colour palette this stage)
	const std::string resPath = getResourcePath();

	//create game object
	Game game;
	//call its update function, which runs until some exit condition
	game.update();

	SDL_Quit();
	return 0;
}