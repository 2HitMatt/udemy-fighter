#ifndef CHARACTERKEYBOARDCONTROLLER
#define CHARACTERKEYBOARDCONTROLLER

#include "CharacterController.h"

class CharacterKeyboardController : public CharacterController
{
	public:
		SDL_Scancode UP, DOWN, LEFT, RIGHT;
		SDL_Scancode A, B, C;
		list<SDL_Scancode> buttons;

		CharacterKeyboardController();
		void update(SDL_Event* e);
};

#endif
