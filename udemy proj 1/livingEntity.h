#ifndef LIVINGENTITY
#define LIVINGENTITY

#include "Entity.h"

//abstract also
class LivingEntity : public Entity
{
public:
	//things that live have health and potentially cause pain
	int hp, hpMax;

	float invincibleTimer = 0;

	virtual void updateInvincibleTimer();

	void draw();

};

#endif