#include "TimeController.h"

int TimeController::PLAY_STATE = 0, TimeController::PAUSE_STATE = 1;

//int timeState;
//Uint32 lastUpdate;
//float dT; //delta time, in seconds (0.001 probably the average)

TimeController::TimeController()
{
	dT = 0;
	lastUpdate = 0;
}

void TimeController::updateTime()
{
	
	if (timeState == PAUSE_STATE)
		dT = 0;
	else
	{
		Uint32 timeDiff = SDL_GetTicks() - lastUpdate;
		dT = timeDiff / 1000.0; //move these milliseconds into the decimal place area
	}
	lastUpdate = SDL_GetTicks();
}
void TimeController::pause()
{
	timeState = PAUSE_STATE;
}
void TimeController::resume()
{
	timeState = PLAY_STATE;
}
void TimeController::reset()
{
	dT = 0;
	lastUpdate = SDL_GetTicks();
}


TimeController TimeController::timerController;