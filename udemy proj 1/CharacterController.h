#ifndef CHARACTERCONTROLLER
#define CHARACTERCONTROLLER

#include "Fighter.h"
#include "FighterMove.h"

class CharacterController{
public:
	Fighter* fighter;
	//TODO 
	list<FighterMove> moves;
	//string of current inputs
	string commands;
	//input timeout timer
	float inputTimeoutTimer = 0;
	float inputMaxTime = 1;

	virtual void update(SDL_Event* e) = 0;
	void addCommand(string command);
	void updateInputTimer();
	void checkCommandsForMoves();
	void doMove(FighterMove);
	void walk(bool dir);
	void checkCommandsForRun(bool dir);
	void run(bool dir);
	void jump();
	void crouch();
	

	virtual void update(SDL_Event* e) = 0;
	virtual void checkEvent(SDL_Event* e) = 0;

};

#endif
