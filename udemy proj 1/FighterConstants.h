#ifndef FIGHTERCONSTANTS
#define FIGHTERCONSTANTS

#include <string>

namespace FC{

	/*fighter state*/
	static int fs_idle = 0, fs_walk = 1, fs_run = 2, fs_walkBackwards = 3, fs_crouch = 4, fs_hurt = 5, fs_inAir = 6, fs_crashed = 7, fs_action = 8;
	
	/*fighter directions*/
	static bool fd_right = true, fd_left = false;

	/*fighter base animations*/
	static std::string fba_idle = "idle", fba_walk = "walk", fba_run = "run", fba_walkBackwards = "walkBackwards", fba_crouch = "crouch", fba_hurt = "hurt", fba_hurtInAir = "hurtInAir", fba_inAir = "inAir", fba_crashed = "crashed";

	static string C_UP = "u", C_DOWN = "d", C_LEFT = "l", C_RIGHT = "r", C_A = "a", C_B = "b", C_C = "c";//add more if need more

	enum FighterHitType{
		fht_small, fht_medium, fht_heavy, fht_unblockable
	};

	enum FighterBlockingType{
		fbt_none, fbt_low, fbt_high, fbt_all
	};
}
#endif